<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityPolicingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_policing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn')->nullable();
            $table->string('requestdate')->nullable();
            $table->string('registered_code')->nullable();
            $table->string('name_focal_person')->nullable();
            $table->string('date')->nullable();
            $table->string('name_of_person_with_issue')->nullable();
            $table->string('province')->nullable();
            $table->string('district')->nullable();
            $table->string('sector')->nullable();
            $table->string('cell')->nullable();
            $table->string('village')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('name_of_spouse')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('is_it_a_land_related_issue')->nullable();
            $table->string('is_it_your_own_issue')->nullable();
            $table->string('land_has_required_documents')->nullable();
            $table->string('issue_details')->nullable();
            $table->string('type_of_land_issue')->nullable();
            $table->string('name_of_the_offender')->nullable();
            $table->string('relation_with_the_offender')->nullable();
            $table->string('status_of_the_issue')->nullable();
            $table->string('sector_where_the_issue_is')->nullable();
            $table->string('issue_registration_date')->nullable();
            $table->string('last_issue_update_from_the_sector')->nullable();
            $table->string('conclusion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_policing');
    }
}
