<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbunziTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abunzi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn')->nullable();
            $table->string('requestdate')->nullable();
            $table->string('registered_code')->nullable();
            $table->string('committee_type')->nullable();
            $table->string('province')->nullable();
            $table->string('district')->nullable();
            $table->string('sector')->nullable();
            $table->string('total_number_of_received_issues')->nullable();
            $table->string('cell')->nullable();
            $table->string('number_of_received_issues_from_male')->nullable();
            $table->string('number_of_received_issues_from_female')->nullable();
            $table->string('solved_issues_in_the_current_month')->nullable();
            $table->string('unsolved_issues_in_the_current_month')->nullable();
            $table->string('number_of_issues_related_to_land')->nullable();
            $table->string('number_of_issues_related_to_heritage')->nullable();
            $table->string('number_of_issues_related_to_border')->nullable();
            $table->string('number_of_issues_related_to_land_gift')->nullable();
            $table->string('number_of_issues_related_to_other_gift')->nullable();
            $table->string('number_of_family_related_land_issues')->nullable();
            $table->string('number_of_other_family_related_issues')->nullable();
            $table->string('number_of_issues_related_to_renting')->nullable();
            $table->string('number_of_land_disputes_related_to_land_renting')->nullable();
            $table->string('number_of_land_disputes_related_to_breach_of_contact')->nullable();
            $table->string('number_of_other_disputes_related_to_breach_of_contract')->nullable();
            $table->string('other_disputes_not_specified')->nullable();
            $table->string('number_of_abunzi_who_were_suspended_due_to_non_performance')->nullable();
            $table->string('number_of_abunzi_fired_due_to_their_misbehavior')->nullable();
            $table->string('number_of_abunzi_resigned_or_not_present_due_to_other_reasons')->nullable();
            $table->string('number_of_cases_mediated_with_consensus_of_disputant')->nullable();
            $table->string('number_of_cases_mediated_with_partial_consensus')->nullable();
            $table->string('number_of_cases_mediated_without_consensus_among_disputants')->nullable();
            $table->string('conclusion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abunzi');
    }
}
