<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityFocalPerson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_focal_persons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn')->nullable();
            $table->string('requestdate')->nullable();
            $table->string('registered_code')->nullable();
            $table->string('type_of_issue')->nullable();
            $table->string('land_type_of_issue')->nullable();
            $table->string('victim')->nullable();
            $table->string('sector_where_the_issue_is')->nullable();
            $table->string('other_type_details')->nullable();
            $table->string('gender_of_victim')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('suspect')->nullable();
            $table->string('gender_of_suspect')->nullable();
            $table->string('province')->nullable();
            $table->string('district')->nullable();
            $table->string('sector')->nullable();
            $table->string('cell')->nullable();
            $table->string('village')->nullable();
            $table->string('date_and_time')->nullable();
            $table->string('short_description_of_the_issue')->nullable();
            $table->string('conclusion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_focal_persons');
    }
}
